//
//  ViewController.h
//  AuthDemo
//
//  Created by Michael Webb on 17/10/2014.
//  Copyright (c) 2014 Jisc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController  <UIWebViewDelegate>

@property (strong, nonatomic) IBOutlet UIWebView *myWebView;


@end

