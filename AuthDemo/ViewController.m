//
//  ViewController.m
//  AuthDemo
//
//  Created by Michael Webb on 17/10/2014.
//  Copyright (c) 2014 Jisc. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize myWebView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self launchLogin];
}

-(void)launchLogin {
    
    NSURL *websiteUrl = [NSURL URLWithString:@"https://auth.data.alpha.jisc.ac.uk/secure/auth.php"];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:websiteUrl];
    myWebView.delegate = self;
    myWebView.scalesPageToFit=true;
    [myWebView loadRequest:urlRequest];
  
    
    
}

- (void)webViewDidFinishLoad:(UIWebView *)theWebView
{
    int a=10;
    NSLog(@"Finished Loading");
    
    NSString *currentURL  = theWebView.request.URL.absoluteString;
    
    NSLog(@"Page %@",currentURL);
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
