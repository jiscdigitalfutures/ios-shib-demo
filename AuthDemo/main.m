//
//  main.m
//  AuthDemo
//
//  Created by Michael Webb on 17/10/2014.
//  Copyright (c) 2014 Jisc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
